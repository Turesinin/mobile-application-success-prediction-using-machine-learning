# Multiple Linear Regression

import pandas as pd
import numpy as np
from sklearn import linear_model
import matplotlib.pyplot as plt

# Importing the datasets

dataset = pd.read_csv('4000_merge_dataset_Final.csv')

X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 6].values

# Encoding the Independent Variable

from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X= LabelEncoder()
X[:,0]=labelencoder_X.fit_transform(X[:,0])
onehotencoder=OneHotEncoder(categorical_features= [0])
X=onehotencoder.fit_transform(X).toarray()

#Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
sc_Y = StandardScaler()
X = sc_X.fit_transform(X)
y=y.reshape(-1,1)
y= sc_Y.fit_transform(y)


# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
poly_reg= PolynomialFeatures (degree = 2)
X_Poly = poly_reg.fit_transform(X_train)

lin_reg=LinearRegression()
lin_reg.fit(X_Poly,Y_train)



y_pred= lin_reg.predict(poly_reg.fit_transform(X_test))





#reg = linear_model.LinearRegression(normalize = True)
#reg.fit(X_train, Y_train)
# Predicting the Test set results
#y_pred= reg.predict(X_test)
#y_pred = sc_y.inverse_transform(y_pred)
#y= sc_y.inverse_transform(y)
#Y_test= sc_y.inverse_transform(Y_test)

print('Intercept: \n', lin_reg.intercept_)
print('Coefficients: \n', lin_reg.coef_)



plt.scatter(Y_test, y_pred)
plt.plot(Y_test, Y_test)
plt.show()

#R^2
from sklearn.metrics import r2_score
r2_score(Y_test, y_pred)

from sklearn.model_selection import cross_val_score
accuracies= cross_val_score(estimator = lin_reg,X= X_train,y=Y_train,cv=12)
acuuracy= accuracies.mean()

#MSE
from sklearn import metrics
print(np.sqrt(metrics.mean_squared_error(Y_test, y_pred)))


#variance
#print('Variance score: %.2f' % lin_reg.score(X_test, Y_test))


