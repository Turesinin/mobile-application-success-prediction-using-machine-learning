#SVR Model for Batsman

import pandas as pd
import numpy as np
#Reading the csv
dataset = pd.read_csv('4000_merge_dataset_Final.csv')
#Deviding X-axis And Y-axis
X = dataset.iloc[:, :-1].values
Y= dataset.iloc[:, 6].values

#Data Preprocessing Starts
#Data Encoding
from sklearn.preprocessing import LabelEncoder , OneHotEncoder
labelencoder_X= LabelEncoder()
X[:,0]=labelencoder_X.fit_transform(X[:,0])
onehotencoder = OneHotEncoder(categorical_features= [0])
X=onehotencoder.fit_transform(X).toarray()

#Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
sc_Y = StandardScaler()
X = sc_X.fit_transform(X)
Y=Y.reshape(-1,1)
Y= sc_Y.fit_transform(Y)

#Spliting the data
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size = 0.4, random_state = 0)

#Applying Random Forest
from sklearn.ensemble import RandomForestRegressor
regressor_R = RandomForestRegressor(n_estimators = 300 , random_state=0)
regressor_R.fit(X_train, y_train)

y_pred=regressor_R.predict(X_test)
y_pred= sc_Y.inverse_transform(y_pred)
y_test= sc_Y.inverse_transform(y_test)
#Grid Search CV
from sklearn.model_selection import GridSearchCV
parameters= [{ 'n_estimators': [1,10,100,200,300]},
              {'n_estimators': [1,10,100,200,300]}]
grid_search = GridSearchCV(estimator = regressor_R,
                           param_grid =parameters,
                           cv=4,
                           n_jobs = -1)
grid_search = grid_search.fit(X_train,y_train)
best_accuracy = grid_search.best_score_
best_parameters= grid_search.best_params_



#Calculating r2
from sklearn.metrics import r2_score
R2= r2_score(y_test,y_pred)

#Calculating Rootmean
from sklearn import metrics
rootMean= np.sqrt(metrics.mean_squared_error(y_test, y_pred))

#Calculating Accuracy

from sklearn.model_selection import cross_val_score
accuracies= cross_val_score(estimator = regressor_R,X= X_train,y=y_train,cv=4)
acuuracy= accuracies.mean()

