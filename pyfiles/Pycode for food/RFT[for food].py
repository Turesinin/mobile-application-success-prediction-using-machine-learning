# SVR

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('food.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 69].values

#encoding
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X= LabelEncoder()
X[:,0]=labelencoder_X.fit_transform(X[:,0])
X[:,1]=labelencoder_X.fit_transform(X[:,1])
onehotencoder=OneHotEncoder(categorical_features= [0])
secondEncoder=OneHotEncoder(categorical_features= [1])
X=onehotencoder.fit_transform(X).toarray()
X=secondEncoder.fit_transform(X).toarray()

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
sc_y = StandardScaler()
X = sc_X.fit_transform(X)
y=y.reshape(-1,1)
y = sc_y.fit_transform(y)

# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

#Fitting Random forest
from sklearn.ensemble import RandomForestRegressor
regressor = RandomForestRegressor(n_estimators = 500 , random_state=0)
regressor.fit(X_train, y_train)

y_pred= regressor.predict(X_test)

#k fold cross validation
from sklearn.model_selection import cross_val_score
accuracies= cross_val_score(estimator = regressor,X= X_train,y=y_train,cv=10)
acuuracy= accuracies.mean()

# Predicting a new result
y_pred = regressor.predict(X_test)
y_pred = sc_y.inverse_transform(y_pred)
y= sc_y.inverse_transform(y)
y_test= sc_y.inverse_transform(y_test)




#R^2
 from sklearn.metrics import r2_score
 r2_score(y_test, y_pred)


from sklearn import metrics
print(np.sqrt(metrics.mean_squared_error(y_test, y_pred)))

plt.scatter(X_test,y_test,color='red')
plt.plot(y_test,y_pred,color='blue')
plt.show()

