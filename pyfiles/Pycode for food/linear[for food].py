# Multiple Linear Regression

import pandas as pd
import numpy as np
from sklearn import linear_model
import matplotlib.pyplot as plt

# Importing the datasets

dataset = pd.read_csv('food.csv')

X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 69].values

# Encoding the Independent Variable
#encoding
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X= LabelEncoder()
X[:,0]=labelencoder_X.fit_transform(X[:,0])
X[:,1]=labelencoder_X.fit_transform(X[:,1])
onehotencoder=OneHotEncoder(categorical_features= [0])
secondEncoder=OneHotEncoder(categorical_features= [1])
X=onehotencoder.fit_transform(X).toarray()
X=secondEncoder.fit_transform(X).toarray()



# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Fitting the Multiple Linear Regression in the Training set

reg = linear_model.LinearRegression(normalize = True)
reg.fit(X_train, Y_train)



# Predicting the Test set results
y_pred= reg.predict(X_test)


print('Intercept: \n', reg.intercept_)
print('Coefficients: \n', reg.coef_)



plt.scatter(Y_test, y_pred)
plt.plot(Y_test, Y_test)
plt.show()

#R^2
from sklearn.metrics import r2_score
r2_score(Y_test, y_pred)

from sklearn.model_selection import cross_val_score
accuracies= cross_val_score(estimator = reg,X= X_train,y=Y_train,cv=12)
acuuracy= accuracies.mean()

#MSE
from sklearn import metrics
print(np.sqrt(metrics.mean_squared_error(Y_test, y_pred)))
#variance
print('Variance score: %.2f' % reg.score(X_test, Y_test))

