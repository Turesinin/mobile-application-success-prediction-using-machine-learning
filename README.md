Mobile applications have reached a new level in the recent past years. Now huge number of mobile applications is being launched in Google Play store. With the increase of application development the market competition has reached to another level. Since most of the mobile applications are free the market strategy and revenue models are still quite unknown to the developers and general people. How the in-app features, app permissions, unique features of any app effects the ratings, download or installations are still not known. An app’s success mostly depends on its ratings and downloads over a certain time period. To know about the mobile application performance and analyze their success we’ve collected application data from Google Play store and tried to run different models to predict their success rate as a path to the success of similar kind of application in near future.

**All Project members:**
1.  Mirza Turesinin
2. Tanzina Mollah
3.  Shakhawat Umma Habiba
4.  Abdullah Md. Humayun Kabir
5.  Md. Ariful Alam Khan Shishir




